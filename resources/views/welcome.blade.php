<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Webster - Responsive Multi-purpose HTML5 Template" />
<meta name="author" content="potenzaglobalsolutions.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Holox Guru</title>

<!-- Favicon -->
<link rel="shortcut icon" href="public/images/HoloxGuru.ico" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link href="https://fonts.googleapis.com/css?family=Patua+One" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="public/css/plugins-css.css" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="public/css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="public/css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="public/css/style.css" />

<!-- Custom -->
<link rel="stylesheet" type="text/css" href="public/css/custom.css" />

<!-- plumber -->
<link rel="stylesheet" type="text/css" href="public/css/plumber.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="public/css/responsive.css" />

<!-- Style customizer -->
<link rel="stylesheet" type="text/css" href="public/css/skins/skin-yellow.css" data-style="styles"/>

<link rel="stylesheet" href="public/css/animations.css" type="text/css">

<script src="https://unpkg.com/scrollreveal"></script>
<script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{url('public/js/eskju.jquery.scrollflow.min.js')}}"></script>


</head>

<body style="overflow-x: hidden">

<div class="wrapper">

<!--=================================
 preloader -->
 <script src="https://kit.fontawesome.com/5b24347517.js" crossorigin="anonymous"></script>

<div id="pre-loader">
    <img src="public/images/pre-loader/loader-03.svg" alt="">
</div>

<!--=================================
 preloader -->

<!--=================================
 header -->
<header id="header" class="header default">
  <div class="menu" id="onepagenav">
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items menuB">
     <div class="container container2">
      <div class="row">
       <div class="col-lg-12 col-md-12">
        <!-- menu logo -->
        <ul class="menu-logo padding0">
            <li>
                <a href="{{ url('/') }}"><img class="img-fluid imgS" src="public/images/logo_header.png"> </a>
                <a id="wts" style="display: none;padding-right:15%" class="float-right" target="_blank" href="tel:2227590940">
              <img id="logo_img" src="{{asset('public/images/boton_llamada.png')}}" style="margin-top:30%;height: 35px!important" alt="logo">
            </a>
            <a id="tel" style="display: none;padding-right:4%" class="float-right" target="_blank" href="https://wa.me/522227590940?text=Me%20gustaría%20solicitar%20información">
              <img id="logo_img" src="{{asset('public/images/boton_whats.png')}}" style="margin-top:30%;height: 35px!important" alt="logo">
            </a>
            <a id="tel" style="display: none;padding-right:4%" class="float-right"  href="https://m.me/holoxguru">
              <img id="logo_img" src="{{asset('public/images/boton_messenger.png')}}" style="margin-top:30%;height: 35px!important" alt="logo">
            </a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="menu-links">
            <li><a class="textoMT aMenu" href="#nosotros">NOSOTROS</a></li>
            <li><a class="textoMT aMenu" href="#servicios">SERVICIOS</a></li>
            <li><a class="textoMT aMenu" href="#contacto">CONTACTO</a></li>
            <li><a class="textoMT aMenu" href="http://holox.guru/blog/">BLOG</a></li>
            <li class="menuI"><a target="_blank" href="https://wa.me/522227590940?text=Me%20gustaría%20solicitar%20información"><span><img src="{{url('public/images/boton_whats.png')}}" width="45" style="margin: 15px 0px" alt="animatiomx"></span></a>
            </li>
            <li class="menuI"><a href="https://m.me/holoxguru"><span><img src="{{url('public/images/boton_messenger.png')}}" width="45" alt="animatiomx"></span></a></li>
            <li class="menuI"><a target="_blank" href="tel:2227590940"><span><img src="{{url('public/images/boton_llamada.png')}}" width="45" alt="animatiomx"></span></a>
            </li>
          </ul>
        </div>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>
<!--=================================
 header -->

<!--=================================
 banner -->

<section>
  <div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="public/images/slider1.jpg" alt="Los Angeles" width="1100" height="500">
    </div>
    <div class="carousel-item">
      <img src="public/images/slider2.jpg" alt="Chicago" width="1100" height="500">
    </div>
    <div class="carousel-item">
      <img src="public/images/slider3.jpg" alt="New York" width="1100" height="500">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <i class="fas fa-angle-left" style="color: black; font-size: 45px"></i>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <i class="fas fa-angle-right" style="color: black; font-size: 45px"></i>
  </a>
</div>
</section>


<!--=================================
 banner -->
<!--=================================
work-process -->
<section id="nosotros">
<div  class="animatedParent">
  <h4 class="text-center textoM2 scrollflow -slide-top -opacity" style="padding-top: 75px !important">Nosotros</h4>
</div>

<div style="text-align: left" class="container animatedParent scrollflow -slide-top -opacity">
<p style="padding: 20px" class="textoM3">Somos un grupo de personas comprometidas en constante crecimiento y evolución, para equilibrar nuestra mente, corazón, cuerpo y espíritu con la finalidad de proporcionar un acompañamiento adecuado a todas y cada una de las personas que confían en nosotros.</p>
</div>

<section id="work-process" class="scrollflow -pop -opacity plumber-work-process split-section theme-bg page-section-ptb" style="background-color: #572983">

  <div class="side-background">
    <img class="img-holder img-cover" data-jarallax='{"speed": 0.6}' src="public/images/nosotros.jpg">
   <!--  <div class="col-lg-5 img-side img-left">
         <div class="img-holder img-cover" data-jarallax='{"speed": 0.6}' style="background-image: url(public/images/nosotros.jpg);">
        </div>
      </div> -->
  </div>
  <div class="container">
      <div class="row justify-content-end marginI" >
      <div class="col-lg-6">
          <div class="feature-text left-icon square shadow mb-30">
              <div class="feature-icon">
                <span style="background-color: transparent"><img width="100%" src="public/images/icono_mision.png"></span>
            </div>
            <div class="feature-info">
              <h2 class="textoVal">Misión</h2>
              <p class="textoMVl">Somos una institución terapéutica-profesional-holística que otorga a la comunidad en general, servicios psicoterapéuticos y holísticos especializados que le permitan a nuestros consultantes tener un adecuado desarrollo físico, mental, energético y espiritual, logrando un equilibrio biopsicosocial y espiritual generando así una vida más plena y de calidad.</p>
            </div>
          </div>
          <div class="feature-text left-icon square shadow mb-30">
              <div class="feature-icon">
                <span style="background-color: transparent"><img width="100%" src="public/images/icono_vision.png"></span>
            </div>
            <div class="feature-info">
              <h2 class="textoVal">Visión</h2>
      <p class="textoMVl">Consolidarnos como un equipo de trabajo, comprometido con su crecimiento intelectual, físico, espiritual y energético para dar una atención amorosa, cálida y adecuada a cada necesidad de las personas que confían en nosotros.</p>
            </div>
          </div>
          <div class="feature-text left-icon square shadow">
              <div class="feature-icon">
                <span style="background-color: transparent"><img width="100%" src="public/images/icono_valores.png"></span>
            </div>
            <div class="feature-info">
            <h2 class="textoVal">Valores</h2>
                  <p class="textoMVl">Individualización, Intencionalidad, Calidad y calidez, Acompañamiento, Humanidad, Compromiso, Innovación y Eficacia</p>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
</section>
<!--=================================
work-process -->
<!--=================================
 about -->

  <section id="servicios" class="service-plumber gray-bg page-section-ptb ">
    <div class="container">
     <div class="row">
      <div class="col-lg-12">
        <div class="animatedParent scrollflow -slide-bottom -opacity"><h4 class="text-center textoM2">Servicios</h4></div>

       <div class="owl-carousel" data-nav-dots="true" data-items="3" data-md-items="2" data-sm-items="2" data-xs-items="2" data-xx-items="1" data-space="5">
        <div>
          <img src="public/images/servicio1.jpg">
        </div>
        <div>
          <img src="public/images/servicio2.jpg">
        </div>
        <div>
          <img src="public/images/servicio3.jpg">
        </div>
        <div>
          <img src="public/images/servicio4.jpg">
        </div>
      </div>
      </div>

      <div class="col-lg-12">
        <div class="animatedParent scrollflow -pop -opacity"><h4 class="text-center textoM2">También contamos con...</h4></div>

                 <div class="accordion gray plus-icon round mb-30 scrollflow -slide-top -opacity">
              <div class="acd-group animatedParent">
                  <a href="#" id="menuS" class="acd-heading backG textoMS"><img id="aks" width="5%" src="public/images/iconoM.png">&nbsp &nbsp Área de la salud</a>
                  <div class="row acd-des">
                      <div class="col-lg-6 col-sm-6">
                        <ul class="list list-unstyled mb-30">
                            <li> <i class="fas fa-circle iconM"></i> HoloBiomagnetismo  </li>
                            <li> <i class="fas fa-circle iconM"></i> HoloPsicosomática</li>
                            <li> <i class="fas fa-circle iconM"></i> Neurobiomagnetismo </li>
                            <li> <i class="fas fa-circle iconM"></i> Biomagnetismo </li>
                            <li> <i class="fas fa-circle iconM"></i> Psicosomática de sobrepeso</li>
                            <li> <i class="fas fa-circle iconM"></i> Constelaciones familiares</li>

                            <li> <i class="fas fa-circle iconM"></i> Biodescodificación  </li>
                            <li> <i class="fas fa-circle iconM"></i> Meridianos</li>
                            <li> <i class="fas fa-circle iconM"></i> Emociones atrapadas  </li>
                            <li> <i class="fas fa-circle iconM"></i> Anclajes </li>
                            <li> <i class="fas fa-circle iconM"></i> Mindfulness</li>
                            <li> <i class="fas fa-circle iconM"></i> Homeopatía </li>
                            <li> <i class="fas fa-circle iconM"></i> Flores de Bach </li>
                            <li> <i class="fas fa-circle iconM"></i> Sales de Schüssler </li>
                            <li> <i class="fas fa-circle iconM"></i> Biomagnetismo veterinario</li>
                            <li> <i class="fas fa-circle iconM"></i> Biomagnetismo emocional</li>
                        </ul>
                      </div>
                      <div class="col-lg-6 col-sm-6">
                        <ul class="list list-unstyled mb-30">
                            <li> <i class="fas fa-circle iconM"></i> Psicoterapia </li>
                            <li> <i class="fas fa-circle iconM"></i> Psicodrama</li>
                            <li> <i class="fas fa-circle iconM"></i> Magnetopuntura</li>
                            <li> <i class="fas fa-circle iconM"></i> Magnetoterapia</li>
                            <li> <i class="fas fa-circle iconM"></i> Hipnosis clínica  </li>
                            <li> <i class="fas fa-circle iconM"></i> Bioenergética  </li>

                            <li> <i class="fas fa-circle iconM"></i> Arquetipos Psico-planetarios  </li>
                            <li> <i class="fas fa-circle iconM"></i> Conflictología</li>
                            <li> <i class="fas fa-circle iconM"></i> Sistemas biológicos  </li>
                            <li> <i class="fas fa-circle iconM"></i> Reprogramación del inconsciente  </li>
                            <li> <i class="fas fa-circle iconM"></i> Meditaciones</li>
                            <li> <i class="fas fa-circle iconM"></i> Sanación con cristales</li>
                            <li> <i class="fas fa-circle iconM"></i> Pentaculos de salud  </li>
                            <li> <i class="fas fa-circle iconM"></i> Biomagnetismo </li>
                            <li> <i class="fas fa-circle iconM"></i> Psicosomática de sobrepeso</li>
                            <li> <i class="fas fa-circle iconM"></i> Hoponopono</li>
                        </ul>
                      </div>
                    </div>
              </div>
              <div class="acd-group animatedParent scrollflow -pop -opacity">
                  <a href="#" id="menuS2" class="acd-heading backG textoMS"><img id="aks2" width="5%" src="public/images/icono_VaM.png">&nbsp &nbsp Área energética</a>
                  <div class="row acd-des">
                      <div class="col-lg-6 col-sm-6">
                        <ul class="list list-unstyled mb-30">
                            <li> <i class="fas fa-circle iconM"></i> Bioenergetica </li>
                            <li> <i class="fas fa-circle iconM"></i> Magnetopuntura (alineación campos auricos)</li>
                            <li> <i class="fas fa-circle iconM"></i> Regresiones </li>
                            <li> <i class="fas fa-circle iconM"></i> Armonización de Chakras</li>
                            <li> <i class="fas fa-circle iconM"></i> Angeloterapia</li>
                            <li> <i class="fas fa-circle iconM"></i> Acupuntura (regeneración campos auricos)</li>
                        </ul>
                      </div>
                      <div class="col-lg-6 col-sm-6">
                        <ul class="list list-unstyled mb-30">
                            <li> <i class="fas fa-circle iconM"></i> Reprogramación de canales energéticos</li>
                            <li> <i class="fas fa-circle iconM"></i> Meditaciones en Chakras</li>
                            <li> <i class="fas fa-circle iconM"></i> Sanación Energetica con cristales </li>
                            <li> <i class="fas fa-circle iconM"></i> Desbloqueos energéticos</li>
                            <li> <i class="fas fa-circle iconM"></i> Registros Akashicos </li>
                            <li> <i class="fas fa-circle iconM"></i> Armonización de espacios con feng-shui </li>
                        </ul>
                      </div>
                    </div>
              </div>
              <div class="acd-group animatedParent scrollflow -pop -opacity">
                  <a href="#" id="menuS3" class="acd-heading backG  textoMS"><img id="aks3" width="5%" src="public/images/icono_MaM.png"> &nbsp &nbsp Área profesional</a>
                  <div class="row acd-des ">
                      <div class="col-lg-6 col-sm-6">
                        <ul class="list list-unstyled mb-30">
                            <li> <i class="fas fa-circle iconM"></i> Bioenergetica </li>
                            <li> <i class="fas fa-circle iconM"></i> Magnetopuntura (alineación campos auricos)</li>
                            <li> <i class="fas fa-circle iconM"></i> Regresiones </li>
                            <li> <i class="fas fa-circle iconM"></i> Armonización de Chakras</li>
                            <li> <i class="fas fa-circle iconM"></i> Angeloterapia</li>
                            <li> <i class="fas fa-circle iconM"></i> Acupuntura (regeneración campos auricos)</li>
                        </ul>
                      </div>
                      <div class="col-lg-6 col-sm-6">
                        <ul class="list list-unstyled mb-30">
                            <li> <i class="fas fa-circle iconM"></i> Númerologia</li>
                            <li> <i class="fas fa-circle iconM"></i> Arquetipos-psicoplanetarios</li>
                            <li> <i class="fas fa-circle iconM"></i> Limpieza y armonización de negocios  </li>
                            <li> <i class="fas fa-circle iconM"></i> Lectura de cartas, tarot, café</li>
                            <li> <i class="fas fa-circle iconM"></i> Feng-Shui para trabajo y negocios </li>
                        </ul>
                      </div>
                    </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 about -->

<!--=================================
 testimonial -->

  <section id="contactoB" class="page-section-ptb parallax contactoPM" style="background: url(public/images/mujer-contacto.jpg);">
    <div class="container">
     <div class="row">
       <div class="col-md-12 text-center">
        <div class="section-title animatedParent">
             <h4 class="textoM2 " style="margin-top: 30px">Contacto</h4>
          </div>
        <div class="row animatedParent">
          <div class="col-lg-4 col-md-4 text-center mb-30 animated swing">
            <div>
               <img width="17%" src="public/images/icono_ubicacion.png">
               <p class="textoM paddC">6 poniente 504, San Pedro Colula<br>Puebla Pue 7247</p>
           </div>
         </div>
         <div class="col-lg-4 col-md-4 text-center mb-30 animated swing">
           <div>
               <img width="17%" src="public/images/icono_tel.png">
               <p class="textoM paddC">2227590940</p>
             </div>
         </div>
         <div class="col-lg-4 col-md-4 text-center mb-30 animated swing">
          <div>
               <img width="17%" src="public/images/icono_correo.png">
               <p class="textoM paddC">info@holox.guru</p>
             </div>
         </div>
         </div>

        </div>
      </div>
      <div class="row">
        <div class="col-md-12">

        </div>
      </div>
    </div>
</section>

<!--=================================
 testimonial -->


 <!--=================================
contact -->

 <section id="contacto" class="plumber-contact white-bg">
  <div class="container">
    <div class="row">
    <div class="col-lg-12">
      <div class="plumber-contact-bg scrollflow -slide-top -opacity">
       <div class="row">
        <div class="col-lg-6 col-md-6 sm-mb-40">
         <div class="contact-3-info">
          <div class="clearfix">
           <div id="formmessage">Success/Error Message Goes Here</div>
           <form id="contactform" role="form" method="post" action="php/contact-form.php">
            <div class="contact-form clearfix animatedParent">
                <div class="section-field  ">
                  <input id="name" type="text" placeholder="Nombre*" class="form-control contactM"  name="name">
                 </div>
                 <div class="section-field ">
                    <input type="email" placeholder="E-mail*" class="form-control contactM" name="email">
                  </div>
                 <div class="section-field ">
                    <input type="text" placeholder="Teléfono*" class="form-control contactM" name="phone">
                  </div>
                  <div class="section-field ">
                    <input type="text" placeholder="Asunto*" class="form-control contactM" name="asunto">
                  </div>
                 <div class="section-field textarea">
                   <textarea class="input-message form-control contactM" placeholder="Mensaje*"  rows="7" name="message"></textarea>
                  </div>
                  <!-- Google reCaptch-->
                 <div class="g-recaptcha section-field clearfix" data-sitekey="6LdSSt0UAAAAAEUGQ6tvrd_nF6zaS9OmvgHS5B6I"></div>
                 <div style="text-align: center" class="">
                  <input type="hidden" name="action" value="sendEmail"/>
                   <button id="submit" name="submit" type="submit" value="Send" class="button contactM"><span> Enviar </span></button>
                   </div>
                  </div>
                </form>
               <div id="ajaxloader" style="display:none"><img class="mx-auto mt-30 mb-30 d-block" src="public/images/pre-loader/loader-02.svg" alt=""></div>
            </div>
          </div>
      </div>
       <div class="col-lg-6 col-md-6">
         <div class="agency-map">
           <div style="width: 100%; height: 560px;" id="map-02" class="g-map" data-type='dark'><iframe src="https://www.google.com/maps/d/u/0/embed?mid=1GoxiMKxyBuU-jvYGe1DqnaWJRmSW2xh3" class="mapa"></iframe></div>
          </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
</section>

<!--=================================
contact -->

<!--=================================
 footer -->

<!--================================= footer -->
<footer class="footer footer-topbar" style="background-image:url({{asset('public/images/f2.jpg')}});background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;">
<div class="container">
    <div class="row pt-5 pb-0">
        <div class="col-lg-2" id="imgfooter">
          <img id="xs-small" class="img-fluid" src="{{asset('public/images/logo-morado-oscuro.png')}}" alt="" style="width:140px;height:140px;">
        </div>
      <div class="col-lg-3" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-20">
          <div class="row d-flex justify-content-center">
            <a href="#" class="d-flex justify-content-center">
              <img id="xs-small2" class="img-fluid" src="{{asset('public/images/footer_ubicacion.png')}}" alt="" style="width:50px;height:50px">
            </a>
          </div>
          <div class="row d-flex justify-content-center text-center" style="color: #572983;font-weight: bolder;">
           <span  id="xs-font2">6 Pte. 504 Centro San<br> Pedro Cholula Pue.</span>
          </div>
        </div>
      </div>
      <div class="col-lg-2" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-20">
          <div class="row d-flex justify-content-center">
            <a href="#" class="d-flex justify-content-center">
              <img id="xs-small2" class="img-fluid" src="{{asset('public/images/footer_llamada.png')}}" alt="" style="width:50px;height:50px">
            </a>
          </div>
          <div class="row d-flex justify-content-center" style="color: #572983;font-weight: bolder;">
                <span  id="xs-font2">22 27 59 09 40</span>
          </div>
        </div>
      </div>
      <div class="col-lg-2" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-20">
            <div id="xs-small3" class="row d-flex justify-content-center">
              <a href="#" class="d-flex justify-content-center">
                <img id="xs-small2" class="img-fluid" src="{{asset('public/images/footer_correo.png')}}" alt="" style="width:50px;height:50px;">
              </a>
            </div>
            <div class="row d-flex justify-content-center" style="color: #572983;font-weight: bolder;">
                <span  id="xs-font2">info@holox.guru</span>
            </div>
        </div>
      </div>
      <div class="col-lg-2" id="elementsfooter">
        <div class="social-icons color-hover text-left text-lg-right mt-40">
             <ul class="clearfix">
              <li><a href="https://www.facebook.com/holoxguru"><img class="img-fluid" src="{{asset('public/images/footer_redes1.png')}}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{asset('public/images/footer_redes2.png')}}" alt=""></a></li>
              <li><a href="#"><img class="img-fluid" src="{{asset('public/images/footer_redes3.png')}}" alt=""></a></li>
             </ul>
        </div>
      </div>
      <div id="separa2" class="container pb-3">
        <span class="fnt-xs float-right" style="color: black;">2020 Todos los derechos reservados ANIMATIOMX</span></div>
    </div>
</div>
</footer>

<!--=================================
 footer -->

</div>



<div id="back-to-top"><a class="top arrow" href="#top" style="background-color:#572983 !important"><i class="fas fa-sort-up"></i> <span>TOP</span></a></div>

<!--=================================
 jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="public/js/css3-animate-it.js"></script>

<!-- plugins-jquery -->
<script src="public/js/plugins-jquery.js"></script>

<!-- plugin_path -->
<script>var plugin_path = 'public/js/';</script>

<!-- Google recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>

<!-- custom -->
<script src="public/js/custom.js"></script>
<script type="text/javascript">
   $('#menuS').hover(
    function(){
      $("#aks").attr("src","public/images/iconoB.png");
    },
    function(){
      $("#aks").attr("src","public/images/iconoM.png");
    }
)
</script>

<script type="text/javascript">
   $('#menuS2').hover(
    function(){
      $("#aks2").attr("src","public/images/icono_VaB.png");
    },
    function(){
      $("#aks2").attr("src","public/images/icono_VaM.png");
    }
)
</script>
<script type="text/javascript">
   $('#menuS3').hover(
    function(){
      $("#aks3").attr("src","public/images/icono_MaB.png");
    },
    function(){
      $("#aks3").attr("src","public/images/icono_MaM.png");
    }
)
</script>
</body>
</html>
